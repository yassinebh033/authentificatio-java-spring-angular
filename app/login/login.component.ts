import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../user';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  ngOnInit() {
  }
nom= new FormControl('');
prenom= new FormControl('');
username= new FormControl('');
password= new FormControl('');
user= new User ();

  constructor(private register : RegisterService,private router: Router) { }
  onClickSubmit(){
    this.user.nom=this.nom.value;
    this.user.prenom=this.prenom.value;
    this.user.username=this.username.value;
    this.user.password=this.password.value;
    console.log(this.user);
this.register.registrer(this.user)
    .subscribe(
      res => {
        console.log("registration");
        console.log(res);
        this.router.navigate(['/signin']);
      },
      err => {
        console.log ("erreur");
      }
    )
    
  }
  
}
