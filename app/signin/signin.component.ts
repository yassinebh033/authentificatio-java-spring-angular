import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { User } from '../user';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  ngOnInit() {
  }


  username= new FormControl('');
  password= new FormControl('');
  user= new User ();
  
    constructor(private auth : AuthService,private router: Router) { }
    sign(){
     
      this.user.username=this.username.value;
      this.user.password=this.password.value;
      console.log(this.user);
  this.auth.login(this.user)
      .subscribe(
        (res: any) => {
         localStorage.setItem('token', res.token);
          // localStorage.setItem('refresh_token', JSON.stringify(res.refresh_token));
            this.router.navigate(['/article']);
        },
        err => {
          console.log ("erreur");
        }
      )
      
    }
    
  }
  
 



