import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import {User} from './user'
import { Router } from '@angular/router';
import{TokenService} from '../app/token.service'


const httpOptions={ headers : new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl="http://localhost:9000/personne/login";




@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private handler : HttpBackend , private router:Router   ) { 
    this.http=new HttpClient(this.handler);

  
  }
  login(user : User ): Observable<User>{
    return this.http.post<User>(apiUrl,user,httpOptions) ;
  }
  
getToken(){
  return localStorage.getItem('token');
}

loggedIn(){
return !! localStorage.getItem('token')
}

logOut(){
localStorage.removeItem('token')
this.router.navigate([''])
}

}
