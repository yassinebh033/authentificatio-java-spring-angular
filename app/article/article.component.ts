import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  constructor(private hetusuer : ArticlesService) { }

  ngOnInit() {
    
  }
  onClickSubmit(){
    this.hetusuer.getAllUsers()
    .subscribe(
      (res: any) => {
        console.log(res);
      },
      err => {
        console.log ("erreur");
      }
    )
  }
}
