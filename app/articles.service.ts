import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import { Router } from '@angular/router';
import{User} from '../app/user'




@Injectable({
  providedIn: 'root'
})

export class ArticlesService {
  user= new User();

   httpOptions1={ headers :new HttpHeaders().append( 'Authorization' , `Bearer ${localStorage.getItem('token')}` ) };
 apiUrl1="http://localhost:9000/personne/all";


  constructor(private http: HttpClient, private handler : HttpBackend , private router:Router   ) { 
    this.http=new HttpClient(this.handler);
    console.log(localStorage.getItem('token'))
  
  }

  getAllUsers(){
    //const header = new HttpHeaders();
    //header.set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.get(this.apiUrl1,this.httpOptions1) ;//{headers: header}
  
  }
  
}
