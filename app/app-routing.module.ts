import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ArticleComponent } from './article/article.component';
import { SigninComponent } from './signin/signin.component';


const routes: Routes = [
  {path:'login', component:LoginComponent},
  {path:'article', component:ArticleComponent},
  {path:'signin', component:SigninComponent},

  {path: '**', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
